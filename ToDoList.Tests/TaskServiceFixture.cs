﻿using Microsoft.Extensions.DependencyInjection;
using ToDoList.Models.Tasks;
using Xunit;

namespace ToDoList.Tests
{
    public class TaskServiceFixture : IClassFixture<DbFixture>
    {
        private readonly ServiceProvider _serviceProvider;

        public TaskServiceFixture(DbFixture fixture)
        {
            _serviceProvider = fixture.ServiceProvider;
        }

        [Fact]
        public async void GetTasksByListIdTest()
        {
            var context = _serviceProvider.GetService<TaskService>();
            int? taskListId = null;
            var result = await context.GetTasksByListIdAsync(taskListId);

            Assert.NotEmpty(result);
            Assert.All(result, t => Assert.Equal(taskListId, t.TaskListId));
        }
    }
}