﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ToDoList.Models;

namespace ToDoList.Tests
{
    public class DbFixture
    {
        public DbFixture()
        {
            var serviceCollection = new ServiceCollection();
            ServiceLocator.RegisterServices(serviceCollection);
            serviceCollection
                .AddDbContext<ToDoListContext>(options => options
                        .UseSqlServer("Server=localhost;Database=ToDoList;" +
                                      "Integrated Security=true;Trusted_Connection=True;" +
                                      "MultipleActiveResultSets=true"),
                    ServiceLifetime.Transient);

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        public ServiceProvider ServiceProvider { get; private set; }
    }
}