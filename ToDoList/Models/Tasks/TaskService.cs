﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ToDoList.Infrastructure;
using Task = ToDoList.Models.Tasks.Task;

namespace ToDoList.Models.Tasks
{
    public class TaskService
    {
        private readonly IRepository<Task> _repository;

        public TaskService(IRepository<Task> repository)
        {
            _repository = repository;
        }

        public async System.Threading.Tasks.Task<List<Task>> GetTasksByListIdAsync(int? id)
        {
            return await _repository
                .SearchNoTrackingAsync(t => t.TaskListId == id)
                .OrderBy(t => t.IsCompleted)
                .ThenBy(t=>t.CreationDate)
                .ToListAsync();
        }

        public async System.Threading.Tasks.Task AddAsync(Task task)
        {
            _repository.Insert(task);
            await _repository.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteByIdAsync(int id)
        {
            _repository.DeleteById(id);
            await _repository.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task<bool> IsTaskExistAsync(int id)
        {
            return await _repository.AnyAsync(t => t.Id == id);
        }

        public async System.Threading.Tasks.Task UpdateAsync(Task entity)
        {
            _repository.Update(entity);
            await _repository.SaveChangesAsync();
        }
    }
}