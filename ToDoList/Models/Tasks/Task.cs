﻿using System;
using ToDoList.Models.TaskLists;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Models.Tasks
{
    public class Task
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsCompleted { get; set; }
        public int? TaskListId { get; set; }

        public virtual TaskList TaskList { get; set; }
    }
}