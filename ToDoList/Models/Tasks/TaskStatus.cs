﻿namespace ToDoList.Models.Tasks
{
    public enum TaskStatus
    {
        Pending = 0,
        Completed = 1
    }
}