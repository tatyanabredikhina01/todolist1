﻿using Microsoft.Extensions.DependencyInjection;
using ToDoList.Dto.Factories;
using ToDoList.Infrastructure;
using ToDoList.Models.TaskLists;
using ToDoList.Models.Tasks;

namespace ToDoList.Models
{
    public static class ServiceLocator
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<TaskService>();
            services.AddTransient<TaskListService>();
            services.AddSingleton<TaskFactory>();
            services.AddSingleton<TaskListFactory>();
        }
    }
}