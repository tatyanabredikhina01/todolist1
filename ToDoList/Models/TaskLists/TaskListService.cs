﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ToDoList.Infrastructure;
using Task = ToDoList.Models.Tasks.Task;


namespace ToDoList.Models.TaskLists
{
    public class TaskListService
    {
        private readonly IRepository<TaskList> _repository;

        public TaskListService(IRepository<TaskList> repository)
        {
            _repository = repository;
        }
        
        public async System.Threading.Tasks.Task<TaskList> GetByIdAsync(int id)
        {
            return await _repository.SearchNoTrackingAsync(l=>l.Id==id).FirstAsync();
        }
        
        public async System.Threading.Tasks.Task<List<TaskList>> GetAllListsAsync()
        {
            return await _repository.GetAllNoTrackingAsync().ToListAsync();
        }

        public async System.Threading.Tasks.Task AddAsync(TaskList list)
        {
            _repository.Insert(list);
            await _repository.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task<bool> IsListExistAsync(int id)
        {
            return await _repository.AnyAsync(t => t.Id == id);
        }

        public async System.Threading.Tasks.Task DeleteByIdAsync(int id)
        {
            _repository.DeleteById(id);
            await _repository.SaveChangesAsync();
        }
    }
}