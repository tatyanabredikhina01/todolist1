﻿using System.Collections.Generic;
using ToDoList.Models.Tasks;

namespace ToDoList.Models.TaskLists
{
    public class TaskList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Task> Tasks { get; set; } = new List<Task>();
    }
}