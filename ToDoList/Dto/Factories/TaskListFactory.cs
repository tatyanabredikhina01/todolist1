﻿using AutoMapper;
using ToDoList.Models.TaskLists;

namespace ToDoList.Dto.Factories
{
    public class TaskListFactory
    {
        private readonly IMapper _mapper;

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<TaskListDto,TaskList >();
                // CreateMap<TaskEditDto, Task>();
            }
        }

        public TaskListFactory(IMapper mapper)
        {
            _mapper = mapper;
        }
        
        public TaskList FromDto(TaskListDto dto)
        {
            return _mapper.Map<TaskList>(dto);
        }
    }
}