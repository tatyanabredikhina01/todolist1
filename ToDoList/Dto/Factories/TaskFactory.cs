﻿using AutoMapper;
using Task = ToDoList.Models.Tasks.Task;
namespace ToDoList.Dto.Factories
{
    public class TaskFactory
    {
        private readonly IMapper _mapper;

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<TaskDto, Task>();
                CreateMap<TaskEditDto, Task>();
            }
        }

        public TaskFactory(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Task FromDto(TaskDto dto)
        {
            return _mapper.Map<Task>(dto);
        }
        
        public Task FromDto(TaskEditDto dto)
        {
            return _mapper.Map<Task>(dto);
        }
    }
}