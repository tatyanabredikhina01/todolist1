﻿namespace ToDoList.Dto
{
    public class TaskListDto
    {
        public string Name { get; set; }
    }
}