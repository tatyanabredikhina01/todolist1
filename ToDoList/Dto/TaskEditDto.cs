﻿namespace ToDoList.Dto
{
    public class TaskEditDto
    {
        public int Id { get; set; }
        public bool IsCompleted { get; set; }
        public string Description { get; set; }
        public int? TaskListId { get; set; }

    }
}