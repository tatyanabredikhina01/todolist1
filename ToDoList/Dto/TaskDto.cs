﻿namespace ToDoList.Dto
{
    public class TaskDto
    {
        public string Description { get; set; }
    }
}