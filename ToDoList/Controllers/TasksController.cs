﻿// using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Dto;
using ToDoList.Models.Tasks;
using TaskFactory = ToDoList.Dto.Factories.TaskFactory;

namespace ToDoList.Controllers
{
    public class TasksController : Controller
    {
        private readonly TaskService _service;
        private readonly TaskFactory _factory;

        public TasksController(
            TaskService service,
            TaskFactory factory)
        {
            _service = service;
            _factory = factory;
        }

        public async Task<IActionResult> GetTasks()
        {
            var tasks = await _service.GetTasksByListIdAsync(null);
            return View(tasks);
        }

        [HttpPost]
        public async Task<IActionResult> AddTask(TaskDto dto)
        {
            var task = _factory.FromDto(dto);
            await _service.AddAsync(task);
            return RedirectToAction("GetTasks");
        }

        [HttpPost]
        public async Task<IActionResult> AddTaskInCurrentList(int listId, TaskDto dto)
        {
            var task = _factory.FromDto(dto);
            await _service.AddAsync(task);
            return RedirectToAction("GetTasksByListId", "TaskLists", new {id = listId});
        }

        [HttpPost]
        public async Task<IActionResult> DeleteTask(int id)
        {
            if (!await _service.IsTaskExistAsync(id)) return NotFound();
            await _service.DeleteByIdAsync(id);
            return RedirectToAction("GetTasks");
        }

        [HttpPost]
        public async Task<IActionResult> EditTask(TaskEditDto dto)
        {
            var task = _factory.FromDto(dto);
            await _service.UpdateAsync(task);
            return dto.TaskListId != null
                ? RedirectToAction("GetTasksByListId", "TaskLists", new {id = dto.Id})
                : RedirectToAction("GetTasks");
        }
    }
}