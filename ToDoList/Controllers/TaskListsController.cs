﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Dto;
using ToDoList.Dto.Factories;
using ToDoList.Models.TaskLists;
using ToDoList.Models.Tasks;

namespace ToDoList.Controllers
{
    public class TaskListsController : Controller
    {
        private readonly TaskListService _taskListService;
        private readonly TaskService _taskService;
        private readonly TaskListFactory _factory;

        public TaskListsController(
            TaskListService taskListService,
            TaskService taskService,
            TaskListFactory factory)
        {
            _taskListService = taskListService;
            _taskService = taskService;
            _factory = factory;
        }

        [HttpGet]
        public async Task<IActionResult> GetTaskLists()
        {
            var taskLists = await _taskListService.GetAllListsAsync(); 
            return View(taskLists);
        }

        [HttpPost]
        public async Task<IActionResult> AddTaskList(TaskListDto dto)
        {
            var taskList = _factory.FromDto(dto);
            await _taskListService.AddAsync(taskList);
            return RedirectToAction("GetTaskLists");
        }

        [HttpGet]
        public async Task<IActionResult> GetTasksByListId(int id)
        {
            var tasks = await _taskService.GetTasksByListIdAsync(id);
            return View(tasks);
        }


        [HttpPost]
        public async Task<IActionResult> DeleteList(int id)
        {
            if (!await _taskListService.IsListExistAsync(id)) return NotFound();
            await _taskListService.DeleteByIdAsync(id);
            return RedirectToAction("GetTaskLists");
        }
    }
}