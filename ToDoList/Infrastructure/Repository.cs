﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ToDoList.Infrastructure
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly ToDoListContext _context;

        public Repository(ToDoListContext context)
        {
            _context = context;
        }

        public IQueryable<TEntity> SearchNoTrackingAsync(Expression<Func<TEntity, bool>> condition)
        {
            return _context.Set<TEntity>().Where(condition).AsNoTracking();
        }

        public IQueryable<TEntity> GetAllNoTrackingAsync()
        {
            return _context.Set<TEntity>().AsNoTracking();
        }

        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> condition)
        {
            return _context.Set<TEntity>().AnyAsync(condition);
        }

        public void Insert(TEntity item)
        {
            _context.Set<TEntity>().Add(item);
        }

        public void Update(TEntity item)
        {
            _context.Set<TEntity>().Update(item);
        }

        public void DeleteById(int id)
        {
            var entity = _context.Set<TEntity>().Find(id);
            if (entity != null)
                _context.Set<TEntity>().Remove(entity);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}