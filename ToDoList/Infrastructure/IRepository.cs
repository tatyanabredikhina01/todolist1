﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ToDoList.Infrastructure
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        IQueryable<TEntity> SearchNoTrackingAsync(Expression<Func<TEntity, bool>> condition);
        IQueryable<TEntity> GetAllNoTrackingAsync();
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> condition);
        void Insert(TEntity item);
        void Update(TEntity item);
        void DeleteById(int id);
        Task<int> SaveChangesAsync();
    }
}