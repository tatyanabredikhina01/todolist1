﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using ToDoList.Models.TaskLists;
using Microsoft.EntityFrameworkCore;
using Task = ToDoList.Models.Tasks.Task;
namespace ToDoList
{
    public class ToDoListContext : DbContext
    {
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskList> TaskLists { get; set; }

        public ToDoListContext(DbContextOptions options): base(options)
        {
            
        }
        // public ToDoListContext(): this(options.UseSqlServer("Server=localhost;Database=ToDoList;Integrated Security=true;Trusted_Connection=True;MultipleActiveResultSets=true"))
        // {
        //     
        // }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Task>().HasKey(t=>t.Id);

            modelBuilder.Entity<Task>(entity =>
            {
                entity.HasOne(d => d.TaskList)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.TaskListId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}